#!/usr/bin/python

import serial, sys, os, time
import argparse
import logging

DEBUG=0
DLE = '\x10'
EOT = '\x04'

def printHex(data):
  for c in data:
    print "%02x" % ord(c),
    print " ",
  print

def readAnswer(what):
  data = ser.readline()
  data = data.strip("\r\n")
  if DEBUG:
    print(what + ' got(' + data + ')')
  return data

def readList():
  print 'Files:'
  data = ser.readlines()
  for line in data:
    line = line.strip("\r\n")
    words = line.split(' ')
    print words[0], ':', words[1], 'bytes'

def readDump():
  data = ser.readlines()
  for line in data:
    line = line.strip("\r\n")
    print line

def saveFile(fileName):
  fileName += ".dump"
  print fileName + ':',
  f = open(fileName, "wb")
  data = ser.readline()
  while len(data):
    c = int(data[:2], 16)
    f.write(chr(c))
    data = data[2:]
  print 'Done'
  f.close()

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--size", help="Get FLASH size", action="store_true")
parser.add_argument("-b", "--bsize", help="Get FLASH block size", action="store_true")
parser.add_argument("-e", "--erase", help="Erase FLASH", action="store_true")
parser.add_argument("-l", "--list", help="List files", action="store_true")
parser.add_argument("-d", "--dump", help="Dump file", action="store_true")
parser.add_argument("-g", "--get", help="Get file", action="store_true")
parser.add_argument("device", type=str, help="serial device")
parser.add_argument('file', nargs=argparse.REMAINDER)
args = parser.parse_args()
if DEBUG:
  print args

ser = serial.Serial(args.device, baudrate=115200)
ser.timeout = 2
time.sleep(0.1)
ser.readline()

if args.erase:
  ser.write('e')
  readAnswer('e')
  sys.exit()

if args.list:
  ser.write('l')
  readList()
  sys.exit()

if args.dump:
  for file in args.file:
    print file + ':'
    ser.write('n' + file + '\n')
    ser.readline()
    ser.write('h\n')
    readDump()
  sys.exit()

if args.get:
  for file in args.file:
    ser.write('n' + file + '\n')
    ser.readline()
    ser.write('g\n')
    saveFile(file)
  sys.exit()

ser.write("f\n")
data = readAnswer('FLASH')
flashSize = 1024*1024*int(data)

if args.size:
  print data + ' Mbytes (' + str(flashSize) + ' bytes)'
  sys.exit()

if args.bsize:
  ser.write("b\n")
  data = readAnswer('blk')
  print data + ' bytes'
  sys.exit()

totalFileSize = 0;
for filename in args.file:
  totalFileSize = totalFileSize + os.path.getsize(filename)

flashSizeBytes = flashSize * 1024 * 1024
if (totalFileSize > flashSizeBytes):
  print("Too many files selsected.\n\tTotal flash size:\t" + "{:>14,}".format(flashSizeBytes) + " bytes\n\tTotal file size:\t" + "{:>14,}".format(totalFileSize) + " bytes")
  sys.exit()

print('Uploading ' + str(len(args.file)) + ' file(s) ...')
for fileName in args.file:
  fileSize = os.path.getsize(fileName)
  print(fileName + " (" + str(fileSize) + " bytes)")
  ser.write('n' + fileName + '\n')
  readAnswer('n')
  ser.write('s' + str(fileSize) + '\n')
  readAnswer('s') 
  f = open(fileName, "rb")
  content = ''
  buf = ''
  for byte in f.read():
    content += '%02x' % ord(byte)
    if byte == EOT:
      buf += DLE
      buf += EOT
    elif byte == DLE:
      buf += DLE
      buf += DLE
    else:
      buf += byte
    if len(buf) == 128:
      ser.write('d'+buf+EOT);
      print(str(len(buf)) + " bytes sent");
      readAnswer('d')
      buf = ''
  ser.write('d'+buf+EOT);
  print(str(len(buf)) + " bytes sent");
  if DEBUG:
    printHex(buf)
  readAnswer('d')
  data = readAnswer('d')
  if data == 'READY':
    print "End of transfer"
  else:
    print "????"
  ser.write('g')
  data = readAnswer('g')
  if data == content:
    print "verified: OK"
  else:
    print "verified: KO"
    print data + ' ! ' + content
ser.write('l')
readList()
ser.close()

