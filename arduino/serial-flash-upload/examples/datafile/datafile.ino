
#include <SerialFlash.h>

#define CSPIN       10
#define LED         2          // Optional (can be set to ZERO)
#define FSIZE       0x1000

char *logName = "log";

void setup()
{
  Serial.begin(115200);
  /*
    if (SerialFlash.exists(logName)) {
      SerialFlash.remove(logName);
    }
  */
}

void loop()
{
  static unsigned long lastRecordTime = -1;
  static unsigned int lastRecord = 0;
  SerialFlashFile flashFile;
  static uint32_t fileSize = 0;
  char buf[10];

  if (millis() - lastRecordTime >= 60000) {
    lastRecordTime = millis();
    if (!SerialFlash.exists(logName)) {
      Serial.println(F("create file"));
      if (SerialFlash.create(logName, FSIZE) != true) {
        Serial.println(F("cannot create file"));
        return;
      }
    }
    Serial.println(F("open file"));
    flashFile = SerialFlash.open(logName);
    if (!flashFile) {
      Serial.println(F("error opening file"));
      return;
    }
    if (lastRecord >= FSIZE) {
      Serial.println(F("file is full"));
    }
    else {
      flashFile.seek(lastRecord);
      size_t len = sprintf(buf, "%lu\n", millis());
      Serial.print(F("write ")); Serial.print(len); Serial.println(F(" bytes"));
      flashFile.write(buf, len);
      flashFile.close();
      lastRecord += len;
    }
  }
}
