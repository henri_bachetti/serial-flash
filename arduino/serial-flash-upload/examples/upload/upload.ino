
#include <SerialFlash.h>
#include <serial-flash-upload.h>

#define CSPIN       10
#define LED         2          // Optional (can be set to ZERO)

Uploader uploader(CSPIN);

void setup()
{
  Serial.begin(115200);
  if (uploader.begin(LED) == false) {
    Serial.println(F("ERR_FLASH"));
  }
}

void loop()
{
  if (Serial.available()) {
    uploader.shell();
  }
}
