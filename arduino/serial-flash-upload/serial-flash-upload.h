
#ifndef __SERIAL_FLASH_UPLOAD_H__
#define __SERIAL_FLASH_UPLOAD_H__

#include <SerialFlash.h>

#define ID_LEN      5

struct flash_size
{
  uint16_t id;
  size_t size;
};

class Uploader
{
  private:
    void eraseAll(void);
    void hexDump(size_t offset, void *s, int len);
    int m_cspin;
    int m_led;
    uint32_t m_size;

  public:
    Uploader(int cspin);
    size_t size(void) {return m_size;}
    bool begin(int ledPin=0);
    void shell(void);
};

#endif
