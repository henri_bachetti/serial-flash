
#include <Arduino.h>
#include "serial-flash-upload.h"

#define FNAME_LEN   13
#define DATA_LEN    128
#define DLE         0x10
#define EOT         0x04

const flash_size flashSizes[] =
{
  0x4015, 16,
  0x7016, 32,
  0x4017, 64,
  0x4018, 128,
  0x4019, 256,
  0x7119, 512,
  0
};

Uploader::Uploader(int cspin) :
  m_cspin(cspin), m_led(0), m_size(0)
{
}

bool Uploader::begin(int ledPin)
{
  uint8_t id[ID_LEN];

  Serial.println(F("Uploader"));
  m_led = ledPin;
  if (m_led) {
    pinMode(m_led, OUTPUT);
  }
  if (!SerialFlash.begin(m_cspin)) {
    return false;
  }
  SerialFlash.readID(id);
  uint16_t flashId = (id[1] << 8) | id[2];
  for (int i = 0 ; flashSizes[i].id != 0 ; i++) {
    if (flashSizes[i].id == flashId) {
      m_size = flashSizes[i].size / 8;
    }
  }
  return true;
}

void Uploader::eraseAll(void)
{
  Serial.print(F("Erasing "));
  SerialFlash.eraseAll();
  while (!SerialFlash.ready()) {
    Serial.print('.');
    delay(500);
    if (m_led) {
      digitalWrite(m_led, HIGH);
    }
    delay(500);
    if (m_led) {
      digitalWrite(m_led, LOW);
    }
  }
  for (uint8_t i = 0; i < 10; i++) {
    delay(100);
    if (m_led) {
      digitalWrite(m_led, HIGH);
    }
    delay(100);
    if (m_led) {
      digitalWrite(m_led, LOW);
    }
  }
  if (m_led) {
    digitalWrite(m_led, HIGH);
  }
  Serial.println(F("\nREADY"));
}

void Uploader::hexDump(size_t offset, void *s, int len)
{
  int i;
  char sbin[10];
  char buff[17];
  unsigned char *pc = (unsigned char*)s;

  for (i = 0; i < len; i++) {
    if ((i % 16) == 0) {
      if (i != 0) {
        Serial.print(F("    ")); Serial.println(buff);
      }
      sprintf(sbin, "%04x", offset);
      offset += 16;
      Serial.print(sbin);
    }
    sprintf(sbin, "%02x", pc[i]);
    Serial.print(" "); Serial.print(sbin);
    if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
      buff[i % 16] = '.';
    } else {
      buff[i % 16] = pc[i];
    }
    buff[(i % 16) + 1] = '\0';
  }
  while ((i % 16) != 0) {
    Serial.print(F("   "));
    i++;
  }
  Serial.print(F("    ")); Serial.println(buff);
}

void Uploader::shell(void)
{
  static SerialFlashFile flashFile;
  static uint32_t fileSize = 0;
  static char fileName[FNAME_LEN];
  char data[DATA_LEN + 1];
  static uint32_t sz;
  static size_t totalSize;
  bool end = false;

  if (!Serial.available()) {
    return;
  }
  char c = Serial.read();
  switch (c) {
    case 'f':
      Serial.println(m_size);
      break;
    case 'b':
      Serial.println(SerialFlash.blockSize());
      break;
    case 'e':
      eraseAll();
      break;
    case 'n':
      sz = Serial.readBytesUntil('\n', fileName, FNAME_LEN);
      fileName[sz] = '\0';
      Serial.print(F("File name: ")); Serial.println(fileName);
      break;
    case 's':
      sz = Serial.readBytesUntil('\n', data, 10);
      data[sz] = '\0';
      fileSize = atoi(data);
      Serial.print(F("Size: ")); Serial.println(fileSize);
      if (SerialFlash.exists(fileName)) {
        SerialFlash.remove(fileName);
      }
      if (SerialFlash.create(fileName, fileSize)) {
        flashFile = SerialFlash.open(fileName);
        if (!flashFile) {
          Serial.println(F("ERR_OPEN"));
          return;
        }
      }
      else {
        Serial.println(F("ERR_CREATE"));
        return;
      }
      break;
    case 'd':
      sz = 0;
      while (1) {
        if (Serial.available()) {
          int c = Serial.read();
          if (c == -1) {
            Serial.print(sz); Serial.println(F(" ERR_CHAR"));
            break;
          }
          if (c == EOT) {
            flashFile.write(data, sz);
            totalSize += sz;
            Serial.print(F("written: ")); Serial.println(totalSize);
            break;
          }
          if (c == DLE) {
            while (!Serial.available());
            c = Serial.read();
            if (c == -1) {
              Serial.print(sz); Serial.println(F(" ERR_DLE"));
              break;
            }
          }
          data[sz++] = c;
        }
      }
      if (totalSize >= fileSize) {
        flashFile.close();
        Serial.println(F("READY"));
      }
      break;
    case 'l':
      SerialFlash.opendir();
      while (1) {
        char fname[FNAME_LEN];
        uint32_t fsize;
        if (SerialFlash.readdir(fname, sizeof(fname), fsize)) {
          Serial.print(fname);
          Serial.print(' ');
          Serial.println(fsize);
        } else {
          break;
        }
      }
      break;
    case 'g':
      flashFile = SerialFlash.open(fileName);
      if (!flashFile) {
        Serial.print(fileName); Serial.println(F(": ERR_OPEN"));
        return;
      }
      sz = flashFile.size();
      while (sz > 0) {
        unsigned long rd = sz;
        if (rd > sizeof(data)) rd = sizeof(data);
        flashFile.read(data, rd);
        for (int i = 0 ; i < rd ; i++) {
          char sbin[3];
          sprintf(sbin, "%02x", data[i]);
          Serial.print(sbin);
        }
        sz = sz - rd;
      }
      break;
    case 'h':
      flashFile = SerialFlash.open(fileName);
      if (!flashFile) {
        Serial.print(fileName); Serial.println(F(": ERR_OPEN"));
        return;
      }
      sz = flashFile.size();
      size_t count = 0;
      while (sz > 0) {
        unsigned long rd = sz;
        if (rd > sizeof(data)) rd = sizeof(data);
        flashFile.read(data, rd);
        data[rd] = '\0';
        hexDump(count, data, rd);
        count += rd;
        sz = sz - rd;
      }
      break;
  }
}
